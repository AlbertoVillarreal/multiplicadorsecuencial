/********************************************************************
* Name:
*	ValorAbsoluto.sv
*
* Description:
* 	This module receives data from the switches in a2 complement and take the absolute value as output.
*
* Inputs:
*	entrada: 						Binary data from switches in a2 complement.
*
* Outputs:
* 	salida: 							Absolute value.
* 	signo: 							Sign of the number.
*
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	3/2/2018 
*
*********************************************************************/
module ValorAbsoluto
#(
	parameter word_length = 16
 )
(

	 //Input Ports
    input [word_length - 1 : 0] entrada,
	 
	 //Output Ports
    output [word_length - 1 : 0] salida,
    output signo
 );
 
	logic [word_length - 1 : 0] salida_log;
	bit signo_log; 
	 always_comb begin: This_is_ValorAbsoluto
	 
	 signo_log=
		(entrada[word_length - 1] == 1)? 1'b1:1'b0;
					
	salida_log= 
		(entrada[word_length - 1] == 1)? (~entrada + 1'b1):entrada;

	 end
	 
	assign signo = signo_log;
	assign salida = salida_log;

endmodule
