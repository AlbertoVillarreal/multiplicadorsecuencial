/********************************************************************
* Name:
*	MultiplicadorSecuencial.sv
*
* Description:
* 	This module is the top module and this do a multiplication, it have
*	a control signal tha triggers the function of different modules
*	that are needed to do the multiplication
*
* Inputs:
*		start:				indicates start of the multiplication
*		multiplier:			multiplier introduced by switches
*		multiplicand:		multiplicand introduced by switches
*		clk:					board clock 50MHz
*		reset:				
*	output :
*		ready:				indicates the end of the multiplication
*	   product:				multiplication result
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	26/2/2018 
*
*********************************************************************/
module MultiplicadorSecuencial
#(
	parameter Word_Length = 8,
	parameter product_length = 16,
	parameter left_flag = 0,
	parameter right_flag = 1,
	parameter Multiplication_is_0 = 8'b0,
	parameter Output_leds_is_0 = 0
)
(
	//Inputs
	input start,
	input [Word_Length - 1 : 0]multiplier,
	input [Word_Length - 1 : 0]multiplicand,
	input clk,
	input reset,
	//Outputs
	output ready,
	output [product_length - 1 : 0]product,
	output [6 : 0] units_display_output,
	output [6 : 0] tens_display_output,
	output [6 : 0] hundreds_display_output,
	output [6 : 0] thousands_units_display_output,
	output [6 : 0] thousands_tens_display_output,
	output sign
);
	//Wires related to multiplications
	logic [Word_Length - 1 : 0]multiplier_bit_wire;
	logic [Word_Length - 1 : 0]multiplicand_bit_wire;
	logic [Word_Length - 1 : 0]mux_output_log;
	logic [product_length - 1 : 0]product_adder_log;
	logic [product_length - 1 : 0]product_register_log;
	logic [product_length - 1 : 0]produc_multiplexor_output_log;
	logic [Word_Length : 0]count_log;
	//Register signals
	bit trigger_log;
	bit star_counting_bit;
	bit load_bit;
	bit shift_bit;
	bit ready_bit;
	bit flush_bit;
	bit data_enable_bit;
	//PLL wire
	bit pll_output_bit;
	//Decoder  wires
	wire [6 : 0] units_display_wire;//units in seven segments
	wire [6 : 0] tens_display_wire;//tens in seven segments
	wire [6 : 0] hundreds_display_wire;//hundreds in seven segments
	wire [6 : 0] thousands_units_display_wire;//units of thousands in seven segments
	wire [6 : 0] thousands_tens_display_wire;//tens of thousands in seven segments
	bit signo_wire;

	////////////////////PLL//////////////////////////////////
	/*PLLMultiplication	PLLMultiplication_inst 
	(
		.areset ( reset ),
		.inclk0 ( clk ),
		.c0 ( pll_output_bit )
	);*/


/////////////Control module/////////////////////////
Control_state_machine
#(
	.Word_Length(8)
)
Control_machine
(
	//Inputs
	.Start(start),
	.ChangeState(trigger_log),
	.clk(clk),
	.reset(reset),
	
	//Outputs
	.StartCounting(star_counting_bit),
	.Load(load_bit),
	.Shift(shift_bit),
	.Ready(ready_bit),
	.Flush(flush_bit),
	.DataEnable(data_enable_bit)

);

/////////////////////////////Counter /////////////////////////////////////////////////	
	Counter_with_function
#(
	// Parameter Declarations
	.MAXIMUM_VALUE(9),
	.NBITS_FOR_COUNTER(CeilLog2(9))
)
Counter
(
		// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(star_counting_bit),
	
	// Output Ports
	.CountOut(count_log) 
);

//////////////////////////Sends triggers to the control module///////////////////////////////////////////////////
Trigger_Machine
Trigger_Multiplcation
(
	//Inputs
	.counter(count_log),
	
	//Output
	.trigger(trigger_log)
);
	
////////////////////////////////Multiplicand register//////////////////////////////////////////////


Entry_register
Its_me_a_left_Register
(
	//Inputs
	.clk(clk),
	.reset(reset),
	.multiplicand(multiplicand ),
	.load_flag(load_bit),
	.shift_flag(shift_bit),
	.right_left_flag(0),
	
	//Output
	.number_output(multiplicand_bit_wire)
);

////////////////////////////////Multiplier register//////////////////////////////////////////////
Entry_register
Its_me_a_right_Register
(
	//Inputs
	.clk(clk),
	.reset(reset),
	.multiplicand(multiplier),
	.load_flag(load_bit),
	.shift_flag(shift_bit),
	.right_left_flag(1),
	
	//Output
	.number_output(multiplier_bit_wire)
);

////////////////////////////////////////////////////////////////////////////////////////
multiplexor
#(
	.entrada(8)
)
Multiplier_multiplexor
(
	// Input Ports
	.Selector(multiplier_bit_wire[0]),
	.Data_0(Multiplication_is_0),
	.Data_1(multiplicand_bit_wire),
	
	// Output Ports
	.Mux_Output(mux_output_log)

);

//////////////////////////////////////////////////////////////////////////////////////
Adder
#(
	.product_data_length(16)
)
Multiplication
(
	//Inputs
	.multiplication(product_register_log),
	.new_bit_multiplication(mux_output_log),
	
	//Outpus
	.product(product_adder_log)
);

//////////////////////////////Register that save the result/////////////////////////////////////////////////////////
 Register_with_clock_enable
#(
	.Word_Length(16)
)
Multiplication_register
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(data_enable_bit),
	.flush(flush_bit),
	.Data_Input(product_adder_log),

	// Output Ports
	.Data_Output(product_register_log)
);

///////////////////////////////////////////////////////////////////////////////////////
multiplexor
#(
	.entrada(16)
)
product_multiplexor
(
	// Input Ports
	.Selector(ready_bit),
	.Data_0(Output_leds_is_0),
	.Data_1(product_register_log),
	
	// Output Ports
	.Mux_Output(produc_multiplexor_output_log)

);
Decodificador
Multiplication_Decoder
(
	//Input ports
	.data_input(produc_multiplexor_output_log),
	
	//Output ports
	.units_display_output(units_display_wire),
	.tens_display_output(tens_display_wire),
	.hundreds_display_output(hundreds_display_wire),
	.thousands_units_display_output(thousands_units_display_wire),
	.thousands_tens_display_output(thousands_tens_display_wire),
	.sign(signo_wire)
	
);

assign product = produc_multiplexor_output_log;
assign ready = ready_bit;
assign units_display_output = units_display_wire;
assign tens_display_output = tens_display_wire;
assign hundreds_display_output = hundreds_display_wire;
assign thousands_units_display_output = thousands_units_display_wire;
assign thousands_tens_display_output = thousands_tens_display_wire;
assign sign = signo_wire;

//------------------------------------Compilator Directives----------------------------------------------------------

/*--------------------------------------------------------------------*/
 /*--------------------------------------------------------------------*/
 /*--------------------------------------------------------------------*/
   
 /*Log Function*/
     function integer CeilLog2;
       input integer data;
       integer i,result;
       begin
          for(i=0; 2**i < data; i=i+1)
             result = i + 1;
          CeilLog2 = result;
       end
    endfunction

/*--------------------------------------------------------------------*/
 /*--------------------------------------------------------------------*/
 /*--------------------------------------------------------------------*/
endmodule