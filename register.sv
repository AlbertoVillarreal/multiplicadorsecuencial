/********************************************************************
* Name:
*	register.sv
*
* Description:
* 	This module is used to save the new shifted value that comes from the 
*	shift_load_register.
*
* Inputs:
*		clk:				Board clock, 50MHz
*		reset:			reset
*		in_number:		new shifted number
*	output :
*		out_number:		new shifted number
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	26/2/2018 
*
*********************************************************************/
module register
#(
	parameter data_length = 8
)
(
	//Inputs
	input clk,
	input reset,
	input [data_length - 1 : 0] in_number,
	//Outputs
	output [data_length - 1 : 0] out_number
);

logic [data_length - 1 : 0]out_numer_logic;

always_ff@(posedge clk, negedge reset)
begin
	if(reset == 1'b0)
		out_numer_logic <= 8'b0;
	else
	out_numer_logic <= in_number;
	
end

assign out_number = out_numer_logic;

endmodule