timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.

module Control_state_machine_TB;


 // Input Ports
 	logic Start = 1;
	logic ChangeState = 0;
	logic clk = 0;
	logic reset = 1;
	
  // Output Ports
  	logic StartCounting;
	logic Load;
	logic Shift;
	logic Ready;
	logic Flush;

Control_state_machine
DUT
(
	//Input ports
	.Start(Start),
	.ChangeState(ChangeState),
	.clk(clk),
	.reset(reset),
	
	//Output ports
	.StartCounting(StartCounting),
	.Load(Load),
	.Shift(Shift),
	.Ready(Ready),
	.Flush(Flush)
);	

/*********************************************************/
initial // Clock generator
  begin
    forever #2 clk = !clk;
  end
/**

/*********************************************************/
initial begin // reset generator
	
	#10 ChangeState = 1;
	#4 ChangeState = 0;
	
	#18 ChangeState = 1;
	#4 ChangeState = 0;
	
	#25 Start = 0;
end

endmodule