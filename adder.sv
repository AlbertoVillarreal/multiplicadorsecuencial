/********************************************************************
* Name:
*	adder.sv
*
* Description:
* 	This module is used to do add the two entry variables.
*
* Inputs:
*		multiplication:				previous multiplication value
*		new_bit_multiplication:		new multilication value
*	output :
*		product:					result value
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	26/2/2018 
*
*********************************************************************/
module Adder
#(
	parameter product_data_length = 16
)
(
	//Inputs
	input[ product_data_length - 1 : 0 ] multiplication,
	input [7 : 0]new_bit_multiplication,
	
	//Outpus
	output[ product_data_length - 1 : 0 ] product
);

logic[ product_data_length - 1 : 0 ] product_log/*synthesis keep*/;

always_comb
begin
	product_log = multiplication + new_bit_multiplication;
end
	assign product = product_log;
	
endmodule