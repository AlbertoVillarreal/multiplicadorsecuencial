/********************************************************************
* Name:
*	Shift_load_register.sv
*
* Description:
* 	This module is used to move one bit to the left or to the rights,
*  it depends if it's a multiplier or multiplicand. It loads the value
*	if the load_flag is on
*
* Inputs:
*		multiplication:		multiplier or multiplicad value	
*		load_flag:				indicates if the value can be loaded
*		shift_flag:				indicates if a shift can be done
*		right_left_flag:		indicates if the shift is to the right or the left
*	output :
*		multiplication_bit:	shifted value
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	26/2/2018 
*
*********************************************************************/
module Shift_load_register
#(
	parameter Multiplication_Word_Length = 8
)
(
	//Inputs
	input [Multiplication_Word_Length - 1 : 0]multiplication,
	input load_flag,
	input shift_flag,
	input right_left_flag,
	
	//Outputs
	output [Multiplication_Word_Length - 1 : 0]multiplication_bit

);

	logic[Multiplication_Word_Length - 1 : 0] multiplication_load_logic;
	
always_comb begin
	//Conditional to see if the value can be loaded
	if( 1 == load_flag )
		multiplication_load_logic = multiplication;
	else if( 1 == shift_flag)//Conditional to see if the shift can be done
	begin
		//Conditional to see iv the shif is to the right or the left
		if( right_left_flag == 1)
			multiplication_load_logic = multiplication_load_logic >> 1;
		else
			multiplication_load_logic = multiplication_load_logic << 1;
	end
	else
		multiplication_load_logic = 0;
end



assign multiplication_bit = multiplication_load_logic;
endmodule