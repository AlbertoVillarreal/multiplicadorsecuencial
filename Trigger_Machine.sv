/********************************************************************
* Name:
*	Trigger_Machine.sv
*
* Description:
* 	This module is used to see counter values and depending of the count
*	it send a trigger to the control module
*
* Inputs:
*		counter:				Counter value
*	output :
*		trigger:				trigger
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	26/2/2018 
*
*********************************************************************/
module Trigger_Machine
#(
	parameter WORD_LENGTH = 4,
	parameter COUNTER_FLAG_1 = 1,
	parameter COUNTER_FLAG_8 = 8
)
(
	//Inputs
	input [WORD_LENGTH - 1 : 0]counter,
	
	//Output
	output trigger
);

bit trigger_bit;
//Switch case that send a one depending of the counter
always_comb
begin
case(counter)
	COUNTER_FLAG_1:
		trigger_bit = 1;
	COUNTER_FLAG_8:
		trigger_bit = 1;
	default
		trigger_bit = 0;
endcase
end

assign trigger = trigger_bit;
endmodule