/********************************************************************
* Name:
*	Entry_register.sv
*
* Description:
* 	This module implements a multiplexor, register and shift_load_register
*	to save the new shifted number and changed for the new one when a 
*	shift trigger come's.
*
* Inputs:
*		clk:					Board clock, 50MHz
*		reset:				reset
*		multiplicand:		multiplicand or multiplier value
*		load_flag:			indicates if the value can be loaded
*		shift_flag:			indicates if a shif can be done
*		right_left_flag:	indicates if the shift is to the right or to the left
*	output :
*		number_output:		new shifted number
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	26/2/2018 
*
*********************************************************************/
module Entry_register
#(
	parameter Multiplication_Word_Length = 8
)
(
	//Inputs
	input clk,
	input reset,
	input [Multiplication_Word_Length - 1 : 0]multiplicand,
	input load_flag,
	input shift_flag,
	input right_left_flag,
	
	//Output
	output [Multiplication_Word_Length - 1 : 0]number_output
);

	logic [Multiplication_Word_Length - 1 : 0]multiplication_log;
	logic [Multiplication_Word_Length - 1 : 0]number_output_log;
	logic [Multiplication_Word_Length - 1 : 0]mux_output_log;
	
multiplexor#(
	.entrada(8)
)
Register_Mux
(
	// Input Ports
	.Selector(load_flag),
	.Data_0(number_output_log),
	.Data_1(multiplicand),
	
	// Output Ports
	.Mux_Output(mux_output_log)

);

	Shift_load_register
	shift
	(
	//Inputs
	.multiplication(mux_output_log),
	.load_flag(load_flag),
	.shift_flag(shift_flag),
	.right_left_flag(right_left_flag),
	
	//Outputs
	.multiplication_bit(multiplication_log)
	);

	register
	registro
	(
	//Inputs
	.clk(clk),
	.reset(reset),
	.in_number(multiplication_log),
	//Outputs
	.out_number(number_output_log)
	);
	
	assign number_output = number_output_log;

endmodule