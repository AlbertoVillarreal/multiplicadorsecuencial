/*
*
*/
/********************************************************************
* Name:
*	BinarioBCD.sv
*
* Description:
* 	The function of this module is to convert from BCD to seven segments
* 	
*
* Inputs:
*	num: 							number in BCD .
*
* Outputs:
* 	display_output: 						seven segments dependind on the input.
*
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	3/2/2018 
*
*********************************************************************/

module BCD_7_segmentos
(
	//Input ports
	input [3:0]num,
	
	//Output ports
	output [6:0]display_output
);

logic [6:0]display_output_log;	//output wire
always_comb begin: ThisIsaBCD7Segments
//Ternary oeperators that are used to see wich segments need to be turned on. Segments are
//turned on with 0.
	   display_output_log = 
		(num==0)?7'b0000001://abcdefg	
		(num==1)?7'b1001111:
		(num==2)?7'b0010010:
		(num==3)?7'b0000110:
		(num==4)?7'b1001100:
		(num==5)?7'b0100100:
		(num==6)?7'b0100000:
		(num==7)?7'b0001111:
		(num==8)?7'b0000000:
		(num==9)?7'b0001100:
		(num==10)?7'b0001000:
		(num==11)?7'b1100000:
		(num==12)?7'b0110001:
		(num==13)?7'b1000010:
		(num==14)?7'b0110000:
		(num==15)?7'b0111000:
				  7'b1111110;
end
	assign display_output = display_output_log;
endmodule
