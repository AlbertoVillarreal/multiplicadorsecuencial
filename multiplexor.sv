module multiplexor#(
	parameter entrada = 8
)

(
	// Input Ports
	input Selector,
	input [entrada - 1:0]Data_0,
	input [entrada - 1:0]Data_1,
	
	// Output Ports
	output [entrada - 1:0]Mux_Output

);

logic [entrada - 1:0]Mux_Output_log;

always_comb begin: ThisIsaMUX

	if (Selector == 1'b1)
		Mux_Output_log = Data_1;
	else
		Mux_Output_log = Data_0;

end// end always

assign Mux_Output = Mux_Output_log;

endmodule