timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.

module MultiplicadorSecuencia_TB;


 // Input Ports
 	bit Start = 1;
	bit clk = 0;
	bit reset ;
	logic [8 : 0]multiplier = 5;
	logic [8 : 0]multiplicand = 10;
  // Output Ports
	bit ready;
	logic [15 : 0]product;
MultiplicadorSecuencial
DUT
(
	//Inputs
	.start(Start),
	.multiplier(multiplier),
	.multiplicand(multiplicand),
	.clk(clk),
	.reset(reset),
	//Outputs
	.ready(ready),
	.product(product)
);

/*********************************************************/
initial // Clock generator
  begin
    forever #2 clk = !clk;
  end
/**

/*********************************************************/
initial begin // reset generator
	#0 reset = 0;
	#5 reset = 1;
end

endmodule