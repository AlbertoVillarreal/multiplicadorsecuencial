timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.

module BinarioBCD_TB;


 // Input Ports
logic [6:0]data_input;
	
  // Output Ports
bit signo;
logic [3:0]units_output;
logic [3:0]tens_output;
logic [3:0]hundreds_output;



BinarioBCD
DUT
(
	// Input Ports
	.data_input(data_input),
	
	//Output ports
	.signo(signo),
	.units_output(units_output),
	.tens_output(tens_output),
	.hundreds_output(hundreds_output)

);	


/*********************************************************/
initial begin // reset generator
	#0 data_input = 0;
	#5 data_input = 16;
end

endmodule