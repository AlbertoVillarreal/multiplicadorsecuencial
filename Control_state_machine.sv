module Control_state_machine
#(
	parameter Word_Length = 8
)
(
	//Inputs
	input Start,
	input ChangeState,
	input clk,
	input reset,
	
	//Outputs
	output bit StartCounting,
	output bit Load,
	output bit Shift,
	output bit Ready,
	output bit Flush,
	output bit DataEnable
);

enum logic [2:0] {IDLE, NO_SHIFT, SHIFT, READY} state, next_state; 

/*------------------------------------------------------------------------------------------*/

/*Asignacion de estado, proceso secuencial*/
always_ff@(posedge clk, negedge reset) begin: Assigning_state

	if(reset == 1'b0)
			state <= IDLE;
	else 
			state <= next_state;

end: Assigning_state



/*Asignacion de estado, proceso secuencial*/
always_comb begin	
	 
	 next_state = IDLE;
	 
	 case(state)
		
			IDLE:
				if(Start == 1'b1)			//Begin multiplication
					next_state = NO_SHIFT;
				else
					next_state = IDLE;					
			NO_SHIFT:
				if(ChangeState == 1'b1)	//Signal received to start shifting
					next_state = SHIFT;
				else
					next_state = NO_SHIFT;
			SHIFT:
				if(ChangeState == 1'b1)	//Signal received when multiplication has finished
					next_state = READY;
				else
					next_state = SHIFT;
			READY:
				if(Start == 1'b0)			//Return to IDLE
					next_state = IDLE;
				else
					next_state = READY;
					
			default:
					next_state = IDLE;

			endcase
end//end always
/*------------------------------------------------------------------------------------------*/
/*Asignación de salidas,proceso combintorio*/
always_comb begin
	StartCounting = 1'b0;
	Load = 1'b0;
	Shift = 1'b0;
	Ready = 1'b0;
	Flush = 1'b0;
	DataEnable = 1'b0;
	
	case(state)
		IDLE: 
			begin
				Flush = 1'b1;
			end
			
		NO_SHIFT: 
			begin
				Flush = 1'b1;
				StartCounting = 1'b1;
				Load = 1'b1;
				DataEnable = 1'b1;
			end
			
		SHIFT:
			begin
				Shift = 1'b1;
				StartCounting = 1'b1;
				DataEnable = 1'b1;
			end
				
		READY:
			begin
				Ready = 1'b1;
			end
				
	default: 		
			begin
				StartCounting = 1'b0;
				Load = 1'b0;
				Shift = 1'b0;
				Ready = 1'b0;
				Flush = 1'b0;
			end

	endcase
end

endmodule