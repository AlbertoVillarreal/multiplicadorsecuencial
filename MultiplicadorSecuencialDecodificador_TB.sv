timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.

module MultiplicadorSecuencia_TB;


 // Input Ports
 	bit Start = 1;
	bit clk = 0;
	bit reset ;
	logic [8 : 0]multiplier = 20;
	logic [8 : 0]multiplicand = 5;
  // Output Ports
	bit ready;
	logic [15 : 0]product;
	logic [6 : 0] units_display_wire;//units in seven segments
	logic [6 : 0] tens_display_wire;//tens in seven segments
	logic [6 : 0] hundreds_display_wire;//hundreds in seven segments
	logic [6 : 0] thousands_units_display_wire;//units of thousands in seven segments
	logic [6 : 0] thousands_tens_display_wire;//tens of thousands in seven segments

	bit signo_wire;

MultiplicadorSecuencial
DUT
(
	//Inputs
	.start(Start),
	.multiplier(multiplier),
	.multiplicand(multiplicand),
	.clk(clk),
	.reset(reset),
	//Outputs
	.ready(ready),
	.product(product),
	.units_display_output(units_display_wire),
	.tens_display_output(tens_display_wire),
	.hundreds_display_output(hundreds_display_wire),
	.thousands_units_display_output(thousands_units_display_wire),
	.thousands_tens_display_output(thousands_tens_display_wire),
	.sign(signo_wire)
);

/*********************************************************/
initial // Clock generator
  begin
    forever #2 clk = !clk;
  end
/**

/*********************************************************/
initial begin // reset generator
	#0 reset = 0;
	#5 reset = 1;
	#20 Start = 0;
	#25 Start = 1;
	#30 Start = 0;
	#35 Start = 1;
	#100 Start = 0;
	#1000 Start = 1;
	
end

endmodule