
 module Register_with_clock_enable
#(
	parameter Word_Length = 16
)
(
	// Input Ports
	input clk,
	input reset,
	input enable,
	input flush,
	input [Word_Length-1:0] Data_Input,

	// Output Ports
	output logic [Word_Length-1:0] Data_Output
);

logic [Word_Length-1:0] Data_Output_log/*synthesis keep*/;

always_ff@(posedge clk or negedge reset) begin:ThisIsARegister
	if(reset == 1'b0) 
		Data_Output_log <= {Word_Length{1'b0}};
	else 
	begin
		if (enable == 1'b1)
			Data_Output_log <= Data_Input;
		if(flush == 1'b1)
			Data_Output_log <= {Word_Length{1'b0}};
	end
end:ThisIsARegister

assign Data_Output = Data_Output_log;

endmodule
